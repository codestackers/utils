<?php

return [
    'plugin' => [
        'name' => 'Utils',
        'description' => 'Utils that make the CMS easier to use.'
    ],
    'validation' =>
    [
        'recaptcha'=>
        [
            'error' => 'Not a valid reCaptcha token'
        ],
    ],   
    'settings' => [
        'label'                         => 'Utils Settings',
        'description'                   => 'Configure the Utils Settings',
        'tab_default'                   => 'Defaults',
        'tab_advanced'                  => 'Advanced',
        'use_repeater_auto_fold_label'  => 'Repeater auto fold in',
        'use_repeater_auto_fold_comment'=> 'Set the repeater blocks to auto fold in',
        'use_resize_pane_label'         => 'Resize pane',
        'use_resize_pane_comment'       => 'Enable resize for cms and page pane',
        'use_language_switch_label'     => 'Language switch',
        'use_language_switch_comment'   => 'Enable fix language ',
        'css_language_button_label'     => 'CSS language button',
        'css_language_button_comment'   => 'Change the look and feel of .ml-btn',
        'recaptcha_key_label'      => 'Recaptcha site key',
        'recaptcha_key_comment'    => 'Get your key or stats at https://www.google.com/recaptcha/admin/',
        'recaptcha_secret_label'    => 'Recaptcha server secret',
        'recaptcha_secret_comment'  => 'Get your key or stats at https://www.google.com/recaptcha/admin/',
    ],
    'permission' => [
        'tab' => 'Codestackers Utils',
        'label' => 'Manage Utils Settings'
    ]
];
