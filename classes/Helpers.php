<?php namespace Codestackers\Utils\Classes;

/**
 * Class with commonly used functions that do not belong to any other helpers
 */
class Helpers 
{
    public static function guid()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }    
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));    
    }

    /**
     * @param $input
     * @return string
     */
    public static function stripInput($input = '')
    {
        return preg_replace('/[^ A-Za-z0-9.,!?]/', '', $input);
    }
}